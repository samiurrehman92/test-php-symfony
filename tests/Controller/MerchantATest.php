<?php
/**
 * Created by PhpStorm.
 * User: Sami Ur Rehman
 * Date: 16-Oct-18
 * Time: 11:24 PM
 */

namespace App\Tests\MerchantA;

use App\Controller\MerchantClass;
use App\Controller\MerchantA;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;


class MerchantATest extends TestCase
{
    static $testMerchantA = null;

    static function setUpBeforeClass()
    {
        if (self::$testMerchantA == null) {
            self::$testMerchantA = new MerchantA("testMerchantA");
        }
    }

    /**
     * @dataProvider transactionsProvider
     */
    public function testProcessTransaction($transactionValue, $expectedResult)
    {
        $result = self::$testMerchantA->processTransaction($transactionValue);
        $this->assertEquals($result, $expectedResult);
    }


    public function transactionsProvider()
    {
        return [
            'first trasnaction' => [
                10, [
                    'transaction_value' => 10,
                    'merchant_payout' => 9.5,
                    'epg_commission' => 0.5,
                    'merchant_name' => 'testMerchantA',
                    'merchant_transactino_no' => 1
                ]
            ],
            'second trasnaction' => [
                20, [
                    'transaction_value' => 20,
                    'merchant_payout' => 19,
                    'epg_commission' => 1,
                    'merchant_name' => 'testMerchantA',
                    'merchant_transactino_no' => 2
                ]
            ],
            'third trasnaction' => [
                30, [
                    'transaction_value' => 30,
                    'merchant_payout' => 30,
                    'epg_commission' => 0,
                    'merchant_name' => 'testMerchantA',
                    'merchant_transactino_no' => 3
                ]
            ],
            'fourth trasnaction' => [
                40, [
                    'transaction_value' => 40,
                    'merchant_payout' => 38,
                    'epg_commission' => 2,
                    'merchant_name' => 'testMerchantA',
                    'merchant_transactino_no' => 4
                ]
            ]
        ];
    }

}