<?php
/**
 * Created by PhpStorm.
 * User: Sami Ur Rehman
 * Date: 16-Oct-18
 * Time: 10:54 PM
 */

namespace App\Tests\MerchantClass;

use App\Controller\MerchantClass;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;


class MerchantClassTest extends TestCase
{
    protected $newMerchant = null;

    protected function setUp(){
        $this->testMerchant = new MerchantClass("TestMerchantName");
    }


    public function testConstructor(){
        $this->assertEquals($this->testMerchant->name, "TestMerchantName");
    }

    public function testProcessTransaction(){
        $result = $this->testMerchant->processTransaction([
            'transaction_value' => 10,
            'merchant_payout' => 5,
            'epg_commission' => 2
        ]);

        $expectedResult = [
            'merchant_name' => 'TestMerchantName',
            'merchant_transactino_no' => 0,
            'transaction_value' => 10,
            'merchant_payout' => 5,
            'epg_commission' => 2
        ];

        $this->assertEquals($result, $expectedResult);

        return $this->testMerchant;
    }


    /**
     * @depends testProcessTransaction
     */
    public function testGetTotals(MerchantClass $testMerchantInstance){
        $this->assertEquals($testMerchantInstance->getTotals(), [
            'total_transactions' => 0,
            'total_transaction_value' => 10,
            'total_epg_commission' => 2
        ]);
    }

}