<?php
/**
 * Created by PhpStorm.
 * User: Sami Ur Rehman
 * Date: 16-Oct-18
 * Time: 11:24 PM
 */

namespace App\Tests\MerchantC;

use App\Controller\MerchantClass;
use App\Controller\MerchantC;

use Symfony\Bundle\FrameworkBundle\Tests\TestCase;


class MerchantCTest extends TestCase
{
    static $testMerchantC = null;

    static function setUpBeforeClass()
    {
        if (self::$testMerchantC == null) {
            self::$testMerchantC = new MerchantC("testMerchantC");
        }
    }

    /**
     * @dataProvider transactionsProvider
     */
    public function testProcessTransaction($transactionValue, $transactionCount, $expectedResult)
    {
        $property = new \ReflectionClass(self::$testMerchantC);
        $property = $property->getProperty('transactionCount');
        $property->setAccessible(true);
        $property->setValue(self::$testMerchantC, $transactionCount);
        
        $result = self::$testMerchantC->processTransaction($transactionValue);
        $this->assertEquals($result, $expectedResult);
    }


    public function transactionsProvider()
    {
        return [
            'first trasnaction' => [
                10, 0, [
                    'transaction_value' => 10,
                    'merchant_payout' => 9.5,
                    'epg_commission' => 0.5,
                    'merchant_name' => 'testMerchantC',
                    'merchant_transactino_no' => 1
                ]
            ],
            'ninth trasnaction' => [
                20, 8, [
                    'transaction_value' => 20,
                    'merchant_payout' => 19,
                    'epg_commission' => 1,
                    'merchant_name' => 'testMerchantC',
                    'merchant_transactino_no' => 9
                ]
            ],
            'tenth trasnaction' => [
                30, 9, [
                    'transaction_value' => 30,
                    'merchant_payout' => 28.5,
                    'epg_commission' => 1.5,
                    'merchant_name' => 'testMerchantC',
                    'merchant_transactino_no' => 10
                ]
            ],
            'eleventh trasnaction' => [
                40, 10, [
                    'transaction_value' => 40,
                    'merchant_payout' => 39,
                    'epg_commission' => 1,
                    'merchant_name' => 'testMerchantC',
                    'merchant_transactino_no' => 11
                ]
            ],
            'fourty ninth trasnaction' => [
                100, 48, [
                    'transaction_value' => 100,
                    'merchant_payout' => 97.5,
                    'epg_commission' => 2.5,
                    'merchant_name' => 'testMerchantC',
                    'merchant_transactino_no' => 49
                ]
            ],
            'fiftieth trasnaction' => [
                200, 49, [
                    'transaction_value' => 200,
                    'merchant_payout' => 195,
                    'epg_commission' => 5,
                    'merchant_name' => 'testMerchantC',
                    'merchant_transactino_no' => 50
                ]
            ],
            'fifty first trasnaction' => [
                40, 50, [
                    'transaction_value' => 40,
                    'merchant_payout' => 39.6,
                    'epg_commission' => 0.4,
                    'merchant_name' => 'testMerchantC',
                    'merchant_transactino_no' => 51
                ]
            ],
            'ninety nineth trasnaction' => [
                1000, 98, [
                    'transaction_value' => 1000,
                    'merchant_payout' => 990,
                    'epg_commission' => 10,
                    'merchant_name' => 'testMerchantC',
                    'merchant_transactino_no' => 99
                ]
            ],
        ];
    }

}