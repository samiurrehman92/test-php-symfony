<?php
/**
 * Created by PhpStorm.
 * User: Sami Ur Rehman
 * Date: 16-Oct-18
 * Time: 11:24 PM
 */

namespace App\Tests\MerchantB;

use App\Controller\MerchantClass;
use App\Controller\MerchantB;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;


class MerchantATest extends TestCase
{
    static $testMerchantB = null;

    static function setUpBeforeClass()
    {
        if (self::$testMerchantB == null) {
            self::$testMerchantB = new MerchantB("testMerchantB");
        }
    }

    /**
     * @dataProvider transactionsProvider
     */
    public function testProcessTransaction($transactionValue, $expectedResult)
    {
        $result = self::$testMerchantB->processTransaction($transactionValue);
        $this->assertEquals($result, $expectedResult);
    }


    public function transactionsProvider()
    {
        return [
            'less than threshold' => [
                2, [
                    'transaction_value' => 2,
                    'merchant_payout' => -0.25,
                    'epg_commission' => 2.25,
                    'merchant_name' => 'testMerchantB',
                    'merchant_transactino_no' => 1
                ]
            ],
            'more than threshold' => [
                100, [
                    'transaction_value' => 100,
                    'merchant_payout' => 97,
                    'epg_commission' => 3,
                    'merchant_name' => 'testMerchantB',
                    'merchant_transactino_no' => 2
                ]
            ],
        ];
    }

}