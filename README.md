TO RUN THE PROJECT:

PRE-DEPENDENCY:
1) Have PHP Installed
2) Have Composer Installed



HOW-TO RUN THE PROJECT:

1) RUN COMMAND "composer install" in the root directly
2) RUN COMMAND "php bin/console server:run" in the root directly
3) OPEN URL "http://localhost:8000/execute" in the browser


HOW-TO RUN TESTS:

1) RUN COMMAND "php bin/phpunit" in the root directly

NOTES:
1) I did not write test for RunnerController intentionally since its not part of the logic. Its just a driver to simulate cases
2) FRAMEWORK USED: Symfony 4