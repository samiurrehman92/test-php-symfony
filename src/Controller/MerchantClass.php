<?php
/**
 * Created by PhpStorm.
 * User: Sami Ur Rehman
 * Date: 15-Oct-18
 * Time: 5:19 AM
 */

namespace App\Controller;


use Symfony\Component\Routing\Tests\Fixtures\AnnotatedClasses\AbstractClass;

class MerchantClass extends AbstractClass
{
    /**
     * @var int
     */
    private $transactionsValueSum = 0;

    /**
     * @var int
     */
    private $epgCommissionSum = 0;

    /**
     * @var int
     */
    protected $transactionCount = 0;

    /**
     * @var string
     */
    public $name = null;


    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param           $transactionDetails
     * @return          mixed
     */
    public function processTransaction($transactionDetails){
        $this->transactionsValueSum += $transactionDetails['transaction_value'];
        $this->epgCommissionSum += $transactionDetails['epg_commission'];

        $transactionDetails['merchant_name'] = $this->name;
        $transactionDetails['merchant_transactino_no'] = $this->transactionCount;
        return $transactionDetails;
    }

    /**
     * @return          array
     */
    public function getTotals(){
        return [
            'total_transactions' => $this->transactionCount,
            'total_transaction_value' => $this->transactionsValueSum,
            'total_epg_commission' => $this->epgCommissionSum
        ];
    }

}