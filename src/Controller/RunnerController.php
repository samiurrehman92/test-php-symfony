<?php
/**
 * Created by PhpStorm.
 * User: Sami Ur Rehman
 * Date: 15-Oct-18
 * Time: 4:23 AM
 */

namespace App\Controller;

use App\Controller\MerchantA;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;


class RunnerController
{
    /**
     * @Route("/execute")
     */
    function index(Request $request)
    {
        $transactions_count = rand(0, 100);
        if (!$request) {
            return new JsonResponse([
                'error' => 'Please provide a valid request!',
            ], 422);
        } else if ($request->get('transactions_count') > 0) {
            $transactions_count = $request->get('transactions_count');
        }

        $merchantAClient = new MerchantA("MerchantA");
        $merchantBClient = new MerchantB("MerchantB");
        $merchantCClient = new MerchantC("MerchantC");
        $merchants = [$merchantAClient, $merchantBClient, $merchantCClient];

        $transaction_results = [];
        for ($transactionNo = 0; $transactionNo < $transactions_count; $transactionNo++) {
            array_push($transaction_results, $merchants[rand(0, 2)]->processTransaction(rand(1, 100)));
        }


        $response = new JsonResponse([
            'total_transactions' => $transactions_count,
            'total_transactions_value' => array_sum(array_column($transaction_results, 'transaction_value')),
            'total_epg_commission' => array_sum(array_column($transaction_results, 'epg_commission')),
            'total_merchants_revenue' => array_sum(array_column($transaction_results, 'merchant_payout')),
            'merchants_summary' => [
                'MerchantA' => $merchantAClient->getTotals(),
                'MerchantB' => $merchantBClient->getTotals(),
                'MerchantC' => $merchantBClient->getTotals()
            ],
            'transactions_summary' => $transaction_results
        ]);
        return $response->setEncodingOptions($response->getEncodingOptions() | JSON_PRETTY_PRINT);
    }

}