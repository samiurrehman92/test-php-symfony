<?php
/**
 * Created by PhpStorm.
 * User: Sami Ur Rehman
 * Date: 15-Oct-18
 * Time: 4:51 AM
 */

namespace App\Controller;

use App\Controller\MerchantClass;

class MerchantC extends MerchantClass
{

    /**
     * @param $transactionValue
     */
    function processTransaction($transactionValue = 0)
    {
        $this->transactionCount++;

        if ($this->transactionCount <= 10) {
            $epgCommission = $transactionValue * 0.05;
        } else if ($this->transactionCount <= 50){
            $epgCommission = $transactionValue * 0.025;
        } else {
            $epgCommission = $transactionValue * 0.01;
        }

        return parent::processTransaction([
            'transaction_value' => $transactionValue,
            'merchant_payout' => $transactionValue - $epgCommission,
            'epg_commission' => $epgCommission,
        ]);
    }

}