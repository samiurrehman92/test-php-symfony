<?php
/**
 * Created by PhpStorm.
 * User: Sami Ur Rehman
 * Date: 15-Oct-18
 * Time: 4:40 AM
 */

namespace App\Controller;

use App\Controller\MerchantClass;

class MerchantA extends MerchantClass
{

    /**
     * @param $transactionValue
     */
    function processTransaction($transactionValue = 0)
    {
        $this->transactionCount++;
        $epgCommission = 0;

        if ($this->transactionCount % 3 > 0) {
            $epgCommission = $transactionValue * 0.05;
        }

        return parent::processTransaction(array(
            'transaction_value' => $transactionValue,
            'merchant_payout' => $transactionValue - $epgCommission,
            'epg_commission' => $epgCommission,
        ));
    }
}