<?php
/**
 * Created by PhpStorm.
 * User: Sami Ur Rehman
 * Date: 15-Oct-18
 * Time: 4:49 AM
 */

namespace App\Controller;

use App\Controller\MerchantClass;

class MerchantB extends MerchantClass
{

    /**
     * @param $transactionValue
     */
    function processTransaction($transactionValue = 0)
    {
        $this->transactionCount++;

        $epgCommission = 2.25;
        if ($epgCommission < ($transactionValue * 0.03)){
            $epgCommission = $transactionValue * 0.03;
        }

        return parent::processTransaction([
            'transaction_value' => $transactionValue,
            'merchant_payout' => $transactionValue - $epgCommission,
            'epg_commission' => $epgCommission,
        ]);
    }

}